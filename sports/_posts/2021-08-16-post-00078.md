---
layout: post
title: "2015년 메이저리그 팀 연봉 순위"
toc: true
---

 2015년 메이저리그 팀 연봉 순위를 알아보겠습니다. LA 다저스가 뉴욕 양키스를 가뿐하게 제치고 팀 연봉 1위에 올랐습니다. 다저스가 팀 연봉이 기허 많으냐면 30위 마이애미 말린스보다 약 3.9배 많습니다. 다저스 팀 연봉으로 팀 4개를 만들 생목숨 있다는 이야기입니다.
 다저스가 연봉이 줄지 않은 이유는 클레이튼 커쇼가 오래 계약으로 $4,000,000에서 $32,571,000으로 연봉이 8배 결과 뛰었고 샌디에고로 트레이드 어떤 맷 캠프의 연봉 $21,250,000 반중간 $18,000,000을 보조해 주기 때문입니다.
 게다가 브라이언 윌슨 방출로 $9.5M을 지급해야하고 마이애미로 트레이드 임계 댄 해런의 연봉 $10M도 다저스가 지불해야합니다. 이 금액만 해도 약 $75.6M이 나오네요.
 뉴욕 앙키스는 지난해와 마찬가지로 팀 페이롤이 많습니다만 소폭 증가했습니다. 작세 3위를 차지했던 필라델피아 필리스는 연봉을 줄이며 7위로 내려갔습니다. 구세 4위였던 보스턴 레드삭스는 파블로 산도발, 핸리 라미레즈를 영입하면서 팀 연봉이 얼마 증가하였습니다.
 자이언츠는 작년 메이저리그 팀 연봉 7위 팀이었습니다. 이해 대형 FA영입은 없었지만 팀 전체적으로 고액 연봉자를 비롯해 연봉이 올랐습니다. 5위 디트로이트는 작년에도 5위였습니다. 6위 워싱턴 내셔널스가 맥스 슈어저 등을 영입하면서 9위에서 6위로 뛰어 올랐네요.
 시애틀 매리너스가 18위에서 11위로 껑충 뛰어올랐습니다. 넬슨 크루즈를 영입한 것이 영향을 주었나 봅니다. 시카고 컵스가 23위에서 13위까지 수직 순위가 상승했습니다. 컵스는 존 레스터를 비롯해 제이슨 해멀스 등을 영입하며 전력을 보강하였습니다. 시카고 화이트삭스도 눈에 띄네요.
 화이트삭스는 20위에서 15위로 소폭 올랐습니다. 멜키 카브레라, 애덤 라로쉬, 데이비드 로버트슨, 제프마사자까지 영입하면서 팀 연봉이 올라갔습니다. 신시네티 레즈는 팀 연봉이 아주 소폭 올랐지만 다른 팀의 연봉이 아주 올라가 순위가 하락했습니다. 샌디에고 파드리스는 파격적인 트레이드를 단행했자만 연봉이 상당히 오른 수준이 아닙니다. 유난스레 맷 캠프 경우는 2015년 다저스가 전액 지원해준다고 봐도 될 정도로 많은 연봉을 보조받고 있습니다.
 2014년 메이저리그 팀 연봉 위지 꼴지가 휴스턴이었는데요, 올해는 마이애미 말린스가 30위를 차지했습니다. 템파베이 레이스가 전연도보다 연봉을 떨어뜨렸지만 28위를 유지하고 있습니다.

 연봉 1위는 다저스

 2015년 메이저리그 팀 연봉 순위
 1위 LA 다저스 $271,608,629
 2위 뉴욕 양키스 $217,758,571
 3위 보스턴 레드삭스 $184,345,996
 4위 샌프란시스코 자이언츠 $173,179,277
 5위 디트로이트 타이거스 $172,792,250
 6위 워싱턴 내셔널스 $162,014,559
 7위 필리델피아 필리스 $146,889,667
 8위 LA 에인절스 $146,341,583
 9위 텍사스 레인저스 $141,733,540
 10위 토론토 블루제이스 $125,915,800
 11위 시애틀 매리너스 $123,225,843
 12위 세인트루이스 카디널스 $122,066,500
 13위 시카고 컵스 $120,337,385

 14위 볼티모어 오리올스 $118,975,833
 15위 시카고 화이트삭스 $118,619,178
 16위 신시네티 레즈 $115,373,953
 17위 캔자스시티 로열스 $112,857,025
 18위 샌디에고 파드리스 $108,387,033
 19위 미네소타 트윈스 $108,262,500
 20위 밀워키 브루어스 $104,237,000
 21위 뉴욕 메츠 $101,856,973
 22위 애틀랜타 브레이브스 $97,086,461
 23위 콜로라도 로키스 $97,069,630
 24위 피츠버그 파이러츠 $90,053,000
 25위 클리블랜드 인디언스 $87,997,101
 26위 애리조나 다이아몬드백스 $86,286,000
 27위 오클랜드 애슬래틱스 $83,889,167
 28위 탬파베이 레이스 $75,794,234
 29위 휴스턴 애스트로스 $71,443,700

 30위 마이애미 말린스 $69,031,500

 2015년 메이저리그 팀 연봉 순위

 1위 LA 다저스
 Los Angeles Dodgers: $271,608,629
 팀 연봉 위치 Highest salaries:

 클레이튼 커쇼 Clayton Kershaw ($32,571,429)
 잭 그레인키 Zack Greinke ($25,000,000)
 애드리안 곤잘레스 Adrian Gonzalez ($21,857,143)
 칼 크로포드 Carl Crawford ($21,357,143)


 2위 뉴욕 양키스
 New York Yankees: $217,758,571
 팀 연봉 위지 Highest salaries:

 CC Sabathia ($24,285,714)

 Mark Teixeira ($23,125,000)
 Alex Rodriguez ($22,000,000)
 Masahiro Tanaka ($22,000,000)
 Jacoby Ellsbury ($21,142,857)


 3위 보스턴 레드삭스
 Boston Red Sox: $184,345,996
 팀 연봉 장소 Highest salaries:

 핸리 라미레즈 Hanley Ramirez ($19,750,000),

 파블로 산도발 Pablo Sandoval ($17,600,000),

 마이크 나폴리 Mike Napoli ($16,000,000),

 데이빗 오티즈 David Ortiz ($16,000,000)


 5위 디트로이트 타이거스
 Detroit Tigers: $172,792,250
 팀 연봉 위치 Highest salaries:

 저스틴 벌랜더 Justin Verlander ($28,000,000)
 미구엘 카브레라 Miguel Cabrera ($22,000,000)
 데이빗 프라이스 David Price ($19,750,000)
 아니발 산체스 Anibal Sanchez ($16,800,000)
 이안 킨슬러 Ian Kinsler ($16,000,000)


 6위 워싱턴 내셔널스
 Washington Nationals: $162,014,559
 팀 연봉 순위 Highest salaries:

 제이슨 워스 Jayson Werth ($21,571,429)

 맥스 슈어저 Max Scherzer ($17,142,857)
 조던 짐머맨 Jordan Zimmermann ($16,500,000)


 7위 필리델피아 필리스
 Philadelphia Phillies: $146,889,667
 팀 연봉 순위 Highest salaries:

 라이언 하워드 Ryan Howard ($25,000,000)
 클리프 리 Cliff Lee ($25,000,000)

 콜 해멀스 Cole Hamels ($23,500,000)


 8위 LA 에인절스
 Los Angeles Angels: $146,341,583
 팀 연봉 순위 Highest salaries:

 조쉬 해밀턴 Josh Hamilton ($25,400,000)
 앨버트 푸홀스 Albert Pujols ($24,000,000)
 C.J. 윌슨 C.J. Wilson ($18,500,000)

 제래드 위버 Jered Weaver ($18,200,000)


 9위 텍사스 레인저스
 Texas Rangers: $141,733,540
 팀 연봉 순위 Highest salaries:

 프린스 필더 Prince Fielder ($24,000,000)
 애드리안 벨트레 Adrian Beltre ($16,000,000)
 엘비스 앤드러스 Elvis Andrus ($15,250,000)
 추신수 Shin-Soo [인천 유소년야구](https://installsash.com/sports/post-00007.html) Choo ($14,000,000)


 10위 토론토 블루제이스
 Toronto Blue Jays: $125,915,800
 팀 연봉 순위 Highest salaries:

 호세 레이예스 Jose Reyes ($22,000,000)
 메모 벌리 Mark Buehrle ($20,000,000)


 11위 시애틀 마리너스

 Seattle Mariners: $123,225,843
 팀 연봉 순위 Highest salaries:

 펠릭스 에르난데스 Felix Hernandez ($24,857,143)

 로빈슨 카노 Robinson Cano ($24,000,000)


 12위 세인트루이스 카디널스
 St. Louis Cardinals: $122,066,500
 팀 연봉 순위 Highest salaries:
애덤 웨인라이트 Adam Wainwright ($19,500,000)
맷 할리데이 Matt Holliday ($17,000,000)
야디어 몰리나 Yadier Molina ($15,200,000)
자니 페렐타 Jhonny Peralta ($15,000,000)


 13위 시카고 컵스

 Chicago Cubs: $120,337,385
 팀 연봉 순위 Highest salaries:

 존 레스터 Jon Lester ($20,000,000)
 에드윈 잭슨 Edwin Jackson ($13,000,000)
 미구엘 몬테로 Miguel Montero ($12,000,000


 14위 볼티모어 오리올스
 Baltimore Orioles: $118,975,833
 팀 연봉 순위 Highest salaries:
애덤 존스 Adam Jones ($13,333,333),
우발도 히메네스 Ubaldo Jimenez ($12,250,000),
크리스 데이비스 Chris Davis ($12,000,000)


 15위 시카고 화이트삭스
 Chicago White Sox: $118,619,178
 팀 연봉 순위 Highest salaries:
존 댕크스 John Danks ($15,750,000)
멜키 카브레라 Melky Cabrera ($13,000,000)
애덤 라로쉬 Adam LaRoche ($12,000,000)


 16위 신시네티 레즈
 Cincinnati Reds: $115,373,953
 팀 연봉 순위 Highest salaries:

 조이 보토 Joey Votto ($14,000,000)
 제이 브루스 Jay Bruce ($12,041,667)
 브랜든 필립스 Brandon Phillips ($12,000,000)


 17위 캔자스시티 로열스
Kansas City Royals: $112,857,025
 팀 연봉 순위 Highest salaries:
알렉스 고든 Alex Gordon ($14,000,000)
 알렉스 리오스 Alex Rios ($9,500,000)


 18위 샌디에고 파드리스
 San Diego Padres: $108,387,033
 팀 연봉 순위 Highest salaries:

 멜빈 업튼 주니어 Melvin Upton Jr. ($15,050,000),

 저스틴 업튼 Justin Upton ($14,708,333)


 19위 미네소타 트윈스
 Minnesota Twins: $108,262,500
 팀 연봉 순위 Highest salaries:

 조 마우어 Joe Mauer ($23,000,000),

 어빈 산타나 Ervin Santana (13,500,000),

 리키 놀라스코 Ricky Nolasco ($12,000,000)


 20위 밀워키 브루어스
 Milwaukee Brewers: $104,237,000
 팀 연봉 순위 Highest salaries:

 아라미스 라미레즈 Aramis Ramirez ($14,000,000)
 라이언 브라운 Ryan Braun ($13,000,000)
 맷 가르자 Matt Garza ($12,500,000)


 21위 뉴욕 메츠
 New York Mets: $101,856,973
 팀 연봉 순위 Highest salaries:

 데이빗 라이트 David Wright ($20,000,000)
 커티스 그랜더슨 Curtis Granderson ($16,000,000)


 22위 애틀랜타 브레이브스
 Atlanta Braves: $97,086,461
 팀 연봉 순위 Highest salaries:
닉 마카키스 Nick Markakis ($11,000,000)
프레디 프리먼 Freddie Freeman ($8,859,375)


 23위 콜로라도 로키스
 Colorado Rockies: $97,069,630
 팀 연봉 순위 Highest salaries:

 트로이 툴로위츠키 Troy Tulowitzki ($20,000,000)
 카를로스 곤잘레스 Carlos Gonzalez ($16,428,571)
 호르헤 데 라 로사 Jorge De La Rosa ($12,500,000)


 24위 피츠버그 파이러츠
 Pittsburgh Pirates: $90,053,000
 팀 연봉 순위 Highest salaries:

 프란시스코 리리아노 Francisco Liriano ($11,666,667)
 앤드류 맥커친 Andrew McCutchen ($10,208,333)
 강정호 Jung Ho Kang ($2,500,000) 팀 내 10위


 25위 클리블랜드 인디언스
 Cleveland Indians: $87,997,101
 팀 연봉 순위 Highest salaries:

 닉 스위셔 Nick Swisher ($15,000,000)
 마이클 본 Michael Bourn ($13,500,000)


 26위 애리조나 다이아몬드백스
 Arizona Diamondbacks: $86,286,000
 팀 연봉 순위 Highest salaries:

 아론 힐 Aaron Hill ($12,000,000)
 브론슨 아로요 Bronson Arroyo ($9,500,000)
 코디 로스 Cody Ross ($9,500,000)—방출 뒤 오클랜드 소속 released by team


 27위 오클랜드 애슬래틱스
 Oakland Athletics: $83,889,167
 팀 연봉 순위 Highest salaries:

 스캇 카즈미어 Scott Kazmir ($13,000,000)
 코코 크리프 Coco Crisp ($11,000,000)


 28위 탬파베이 레이스
 Tampa Bay Rays: $75,794,234
 팀 연봉 순위 Highest salaries:

 에반 롱고리아 Evan Longoria ($11,000,000)
 제임스 로니 James Loney ($8,666,667)


 29위 휴스턴 애스트로스
 Houston Astros: $71,443,700
 팀 연봉 순위 Highest salaries:
 스콧 펠드먼 Scott Feldman ($11,000,000)
 제드 라우리 Jed Lowrie ($8,000,000)
 콜비 라스무스 Colby Rasmus ($8,000,000)


 30위 마이애미 말린스
 Miami Marlins: $69,031,500
 팀 연봉 순위 Highest salaries:

 맷 레이토스 Mat Latos ($9,400,000)
 마틴 프라도 Martin Prado ($8,000,000)


 [Copyright ⓒ BaseBallGEN 무단전재 및 재배포 금지]

 Reference
 [1] 2015 Opening Day Payrolls, SI.com
